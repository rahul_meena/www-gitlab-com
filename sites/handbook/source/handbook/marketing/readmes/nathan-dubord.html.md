---
layout: markdown_page
title: "Nathan Dubord's README"
description: "Learn more about working with Nathan Dubord"
---

## Nathan Dubord's README

**Nathan Dubord, Frontend Engineer** 

This page is intended to help others understand what it might be like to work with me, especially people who haven’t worked with me before.

Please feel free to contribute to this page by opening a merge request.

## Related pages

* [GitLab](https://gitlab.com/ndubord)
* [LinkedIn](https://www.linkedin.com/in/nathandubord/)

## About me

Hey! 🙋‍♂️ I'm Nate, or Nathan, whichever you prefer. I'm a designer turned dev who's spent the last 7 years building cool SaaS products: Superna Eyeglass, [SurveyMonkey Apply](https://apply.surveymonkey.com/), [SurveyMonkey Market Research](https://www.surveymonkey.com/market-research/solutions/).

Here's a bit about me:
- 🇨🇦 I live in Ottawa, Ontario, Canada.
- 📐 I have an obsession with design: digital, architecture, interior, you name it.
- ⚽ My evenings and weekends are generally filled with sports: Soccer, Volleyball, Ultimate Frisbee, Skiing, Cycling, Golf.
- 🚀 I love rockets (please don't ask about my NASA trip).
- 🏎️ My goal this year is to get my amateur racing license.
- 💩 I can be a goof, you've been warned!

## Strengths

- I always try to understand the big picture and question why. I'm a strong believer in simple, and making data driven decisions.
- I love bringing people together, often being the bridge between teams.

## Weaknesses

- I can be... optimistic, when committing to deadlines. Unfortunately I've become good friends with the ol' all-nighter.
- Two words, "Uber Eats".

## Communicating with me

I'm pretty informal, message me on slack, send an email, or tag me on a thread and I'll respond as soon as I can. I'm also a big fan of impromptu zoom calls, just make sure we document the end result so everyone's on the same page.